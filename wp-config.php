<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bevyxxa_wp');

/** MySQL database username */
define('DB_USER', 'bevyxxa_wp_user');

/** MySQL database password */
define('DB_PASSWORD', 'bevyxxa_wp_pass');

/** MySQL hostname */
define('DB_HOST', 'ec2-52-2-75-89.compute-1.amazonaws.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q9H-D_LiFWx!sd8hz{~}T2RVOokyqBuqq)St+RTwV%-xvItT%C.-^7$ eUdJgcoY');
define('SECURE_AUTH_KEY',  '# NL7r v L8&s;U:O2Kq]V6q=$AX<&*X(>e9fuO3klM!:0uCT>e2+*{|*6qwUB0Z');
define('LOGGED_IN_KEY',    'Rui7XFQr-+n!5@gP+!Z<~/+KL}y@bG=r&_$>bNGqJ=b E]P^=#=4ch.J7kY}-J&B');
define('NONCE_KEY',        'IAa]u64GD@g{0iAixxxul=9aN*#xJr8abZL5+fDcc{e:!@b b/tFST3H]tcn,XCr');
define('AUTH_SALT',        '|^(kJ_0qFB0V{:>+qpcEws{?=Q/V dgN#1S-,C5f<#G$<8kxDxTk5#z}=6:A>Kqx');
define('SECURE_AUTH_SALT', 'sXG6,N45XXYCR|8dyfF~v+?;9z`>K-0/IE%dc|HO,+,E!|k_3#i`qJz*F(5Ct+a9');
define('LOGGED_IN_SALT',   '9(BU,v`wg0|@~a!+L;;YfQ!+h}`T-S,iCbX:}mIDL{M4Ao<?P+iYd+2].qin|K/9');
define('NONCE_SALT',       'z~doK/xV7d-S}XV!?{-)+jjvq|SSsYE5xM4.=F$=T_hYzo=dg HFG|)-^=o-13~z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
