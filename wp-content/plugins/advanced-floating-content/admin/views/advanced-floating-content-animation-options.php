<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-fafcing aspects of the plugin.
 *
 * @link       http://www.codetides.com/
 * @since      3.0
 *
 * @pafckage    Advanced_Floating_Content
 * @subpafckage Advanced_Floating_Content/admin/views
 */
?>
<div class="afc-panel">                        	
	<?php 
	
	$options = array(
					''=>'Select Any Animation',
					
					'groupstart1'=>'Attention Seekers',
					'bounce'=>'bounce',
					'flash'=>'flash',
					'pulse'=>'pulse',
					'rubberBand'=>'rubberBand',
					'shake'=>'shake',
					'swing'=>'swing',
					'tada'=>'tada',
					'wobble'=>'wobble',
					'jello'=>'jello',
					'groupend1'=>'Attention Seekers',
					
					'groupstart2'=>'Bouncing Entrances',
					'bounceIn'=>'bounceIn',
					'bounceInDown'=>'bounceInDown',
					'bounceInLeft'=>'bounceInLeft',
					'bounceInRight'=>'bounceInRight',
					'bounceInUp'=>'bounceInUp',
					'groupend2'=>'Bouncing Entrances',
					
					'groupstart3'=>'Bouncing Exits',
					'bounceOut'=>'bounceOut',
					'bounceOutDown'=>'bounceOutDown',
					'bounceOutLeft'=>'bounceOutLeft',
					'bounceOutRight'=>'bounceOutRight',
					'bounceOutUp'=>'bounceOutUp',
					'groupend3'=>'Bouncing Exits',
					
					'groupstart4'=>'Fading Entrances',
					'fadeIn'=>'fadeIn',
					'fadeInDown'=>'fadeInDown',
					'fadeInDownBig'=>'fadeInDownBig',
					'fadeInLeft'=>'fadeInLeft',
					'fadeInLeftBig'=>'fadeInLeftBig',					
					'fadeInRight'=>'fadeInRight',
					'fadeInRightBig'=>'fadeInRightBig',
					'fadeInUp'=>'fadeInUp',
					'fadeInUpBig'=>'fadeInUpBig',					
					'groupend4'=>'Fading Entrances',
					
					'groupstart5'=>'Fading Exits',
					'fadeOut'=>'fadeOut',
					'fadeOutDown'=>'fadeOutDown',
					'fadeOutDownBig'=>'fadeOutDownBig',
					'fadeOutLeft'=>'fadeOutLeft',
					'fadeOutLeftBig'=>'fadeOutLeftBig',					
					'fadeOutRight'=>'fadeOutRight',
					'fadeOutRightBig'=>'fadeOutRightBig',
					'fadeOutUp'=>'fadeOutUp',
					'fadeOutUpBig'=>'fadeOutUpBig',					
					'groupend5'=>'Fading Exits',
					
					'groupstart6'=>'Flippers',
					'flip'=>'flip',
					'flipInX'=>'flipInX',
					'flipInY'=>'flipInY',
					'flipOutX'=>'flipOutX',
					'flipOutY'=>'flipOutY',			
					'groupend6'=>'Flippers',
					
					'groupstart7'=>'Lightspeed',
					'lightSpeedIn'=>'lightSpeedIn',
					'lightSpeedOut'=>'lightSpeedOut',	
					'groupend7'=>'Lightspeed',
					
					'groupstart8'=>'Rotating Entrances',
					'rotateIn'=>'rotateIn',
					'rotateInDownLeft'=>'rotateInDownLeft',
					'rotateInDownRight'=>'rotateInDownRight',
					'rotateInUpLeft'=>'rotateInUpLeft',
					'rotateInUpRight'=>'rotateInUpRight',			
					'groupend8'=>'Rotating Entrances',
					
					'groupstart9'=>'Rotating Exits',
					'rotateOut'=>'rotateOut',
					'rotateOutDownLeft'=>'rotateOutDownLeft',
					'rotateOutDownRight'=>'rotateOutDownRight',
					'rotateOutUpLeft'=>'rotateOutUpLeft',
					'rotateOutUpRight'=>'rotateOutUpRight',			
					'groupend9'=>'Rotating Exits',
					
					'groupstart10'=>'Sliding Entrances',
					'slideInUp'=>'slideInUp',
					'slideInDown'=>'slideInDown',
					'slideInLeft'=>'slideInLeft',
					'slideInRight'=>'slideInRight',						
					'groupend10'=>'Sliding Entrances',
					
					'groupstart11'=>'Sliding Exits',
					'slideOutUp'=>'slideOutUp',
					'slideOutDown'=>'slideOutDown',
					'slideOutLeft'=>'slideOutLeft',
					'slideOutRight'=>'slideOutRight',						
					'groupend11'=>'Sliding Exits',
					
					'groupstart12'=>'Zoom Entrances',
					'zoomIn'=>'zoomIn',
					'zoomInDown'=>'zoomInDown',
					'zoomInLeft'=>'zoomInLeft',
					'zoomInRight'=>'zoomInRight',		
					'zoomInUp'=>'zoomInUp',
					'groupend12'=>'Zoom Entrances',
					
					'groupstart13'=>'Zoom Exits',
					'zoomOut'=>'zoomOut',
					'zoomOutDown'=>'zoomOutDown',
					'zoomOutLeft'=>'zoomOutLeft',
					'zoomOutRight'=>'zoomOutRight',		
					'zoomOutUp'=>'zoomOutUp',
					'groupend13'=>'Zoom Exits',
					
					'groupstart14'=>'Specials',
					'hinge'=>'hinge',
					'jackInTheBox'=>'jackInTheBox',
					'rollIn'=>'rollIn',
					'rollOut'=>'rollOut',
					'groupend14'=>'Specials',
					
					);
	
	?>
    
    <div class="afc-panel-div">
        <label for="bafckground_color" style="width: 100%;text-align: left"><?php _e('Load Animation','advanced-floating-content')?></label>
        <select style="width:99%; display:block;    margin-left: 0px" name="ct_afc_animation" id="ct_afc_animation">
		
                <?php
					$opt_animate = "";
                    foreach($options as $key => $value) { 	
						if ($key==get_text_value(get_the_ID(),'ct_afc_animation','fixed')) {
							$selected_ani = 'selected="selected"';
						}
						else{
							$selected_ani = '';
						}	
					
						if (strpos($key, 'groupstart') !== false) {
							echo $value;
							$opt_animate .='<optgroup label="'.$value.'">';							
						}
						else if (strpos($key, 'groupend') !== false) {
							$opt_animate .='</optgroup>';							
						}
						else{
							$opt_animate .='<option value="'.$key.'" '.$selected_ani.'>'.$value.'</option>';	
						}
                   }
				   
				   echo $opt_animate;
				?>					
            </select>        
    </div>
	
	<div class="afc-panel-div">
        <label for="bafckground_color" style="width: 100%;text-align: left"><?php _e('Close Animation','advanced-floating-content')?></label>
        <select style="width:99%; display:block;    margin-left: 0px" name="ct_afc_animation_close" id="ct_afc_animation_close">
		
                <?php
					$opt_close_animate = "";
                    foreach($options as $key => $value) { 	
						if ($key==get_text_value(get_the_ID(),'ct_afc_animation_close','fixed')) {
							$selected_close_ani = 'selected="selected"';
						}
						else{
							$selected_close_ani = '';
						}	
					
						if (strpos($key, 'groupstart') !== false) {
							echo $value;
							$opt_close_animate .='<optgroup label="'.$value.'">';							
						}
						else if (strpos($key, 'groupend') !== false) {
							$opt_close_animate .='</optgroup>';							
						}
						else{
							$opt_close_animate .='<option value="'.$key.'" '.$selected_close_ani.'>'.$value.'</option>';	
						}
                   }
				   
				   echo $opt_close_animate;
				?>					
            </select>        
    </div>
</div>