<?php

// enqueue the child theme stylesheet

Function wp_schools_enqueue_scripts() {
wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
wp_enqueue_style( 'childstyle' );
}
add_action( 'wp_enqueue_scripts', 'wp_schools_enqueue_scripts', 11);

/**
 * Disable auto paragraphs and line-break
 * http://www.computertechtips.net/191/disable-automatic-paragraphs-and-line-breaks-in-wordpress-posts/
 */
remove_filter('the_content', 'wpautop');